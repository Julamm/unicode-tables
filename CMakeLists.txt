cmake_minimum_required(VERSION 3.5)
project(UnicodeTerminals)

include_directories(src/include)

option(build_tests "build unit-tests (requires Catch)" ON)
option(build_example "build example" ON)

file(GLOB uct_interface "src/include/uct/*.hpp")
file(GLOB boxer_interface "src/include/boxer/*.hpp")
file(GLOB boxer_lib "src/lib/*.cpp")
file(GLOB clp_interface "src/include/clp/*.hpp")

add_library(uct INTERFACE ${uct_interface} "src/include/uct/unicode_widths.txt" "src/include/uct/emoji_sequences.txt")
target_compile_features(uct INTERFACE cxx_std_20)
target_include_directories(uct INTERFACE
$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src/include>
$<INSTALL_INTERFACE:include>)

add_library(clp INTERFACE ${clp_interface})
target_link_libraries(clp INTERFACE uct)
target_compile_features(clp INTERFACE cxx_std_20)
target_include_directories(uct INTERFACE
$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src/include>
$<INSTALL_INTERFACE:include>)

add_library(boxer ${boxer_lib})# INTERFACE ${boxer_interface})
target_link_libraries(boxer INTERFACE uct)
target_compile_features(boxer PUBLIC cxx_std_20)
target_include_directories(boxer INTERFACE
$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src/include>
$<INSTALL_INTERFACE:include>)

install(FILES ${uct_interface} "src/include/uct/unicode_widths.txt"  "src/include/uct/emoji_sequences.txt" DESTINATION "include/uct")
install(TARGETS uct
	EXPORT uct_lib_targets)
install(EXPORT uct_lib_targets
	FILE uct.cmake
	NAMESPACE uct::
	DESTINATION lib/cmake/uct)
install(FILES "uct-config.cmake"
	DESTINATION lib/cmake/uct)

install(FILES ${clp_interface} DESTINATION "include/clp")
install(TARGETS clp
	EXPORT clp_lib_targets)
install(EXPORT clp_lib_targets
	FILE clp.cmake
	NAMESPACE clp::
	DESTINATION lib/cmake/clp)
install(FILES "clp-config.cmake"
	DESTINATION lib/cmake/clp)


install(FILES ${boxer_interface} DESTINATION "include/boxer")
install(TARGETS boxer
	EXPORT boxer_lib_targets)
install(EXPORT boxer_lib_targets
	FILE boxer.cmake
	NAMESPACE boxer::
	DESTINATION lib/cmake/boxer)
install(FILES "boxer-config.cmake"
	DESTINATION lib/cmake/boxer)

add_executable(strwidth "src/strwidth/main.cpp")
target_compile_features(strwidth PUBLIC cxx_std_20)
target_link_libraries(strwidth uct boxer)

if (build_tests)
	find_package(Catch2 REQUIRED)
	enable_testing()

	file(GLOB test_sources "src/test/*/*.cpp")

	add_executable(unittests "src/test/main.cpp" ${test_sources})
	target_compile_features(unittests PUBLIC cxx_std_20)
	target_link_libraries(unittests uct boxer clp Catch2::Catch2)
	add_test(uct-tests unittests)
endif()
if (build_example)
	file(GLOB example_sources "src/example/*.cpp")
	add_executable(example ${example_sources})
	target_compile_features(example PUBLIC cxx_std_20)
	target_link_libraries(example uct boxer)
endif()
