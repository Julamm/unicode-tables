Terminal Utils
==============

This project consists of three subcomponents:

* UniCode-Tools (UCT), which provides provides tools to deal with unicode. Most importantly it provides `string_width` which computes the expected display-width of a string in a unicode-compliant terminal-emulator.
  The reference-emulator uses Wezterm with JuliaMono as main-font as most alternatives lack support in certain other areas. (Konsole for example does not correctly displaying zero-width-joiner-based emojis.)
* Boxer, a library to draw boxes do styled printing in terminals that support ANSI-escape codes.
* Command Line Parser (CLP), a command-line parser that started as an independent project but was merged here to improve its unicode-support and collect more terminal-support tools in one place.

The entire codebase asumes the use of UTF8 stored in `std::string_view`-compatible string-containers (most notably `std::string`).
In particular there is no support for `std::u8string` planned.

Lincensing
==========

LGPLv3 or, at your option, any later version.
For the header-only components (Unicode-Width and CLP) this does not put any restrictions on use as long as you do not modify the libraries themselves.
If you do, you have to make the modified versions available under the same terms.

(This may change in the future, though previously released version will remain available under LGPLv3.)
