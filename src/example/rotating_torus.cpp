#include "rotating_torus.hpp"

#include <algorithm>
#include <cmath>

#include <boxer/boxer.hpp>
#include <boxer/heatmap.hpp>
#include <cstddef>
#include <tuple>

namespace {
constexpr double to_d(std::size_t n) { return static_cast<double>(n); }


constexpr auto dist(std::floating_point auto... x_i) {
	return std::sqrt(((x_i * x_i) + ...));
}

constexpr auto torus_sdf(unsigned index, double outer_radius = 0.4, double thickness = 0.15) {
	return [=](double x, double y, double z) {
		const double xy_d = dist(x, y) - outer_radius;
		const double d = dist(xy_d, z);
		return std::pair{d - thickness / 2, index};
	};
}

constexpr auto deathstar_sdf(unsigned index) {
	return [index](double x, double y, double z) {
		auto big = dist(x, y, z) - 0.2;
		auto small = (dist(x - 0.15, y - 0.30, z + 0.30) - 0.3) * -1;

		return std::pair{std::max(big, small), index};
	};
}

constexpr std::invocable<double, double, double> auto
sdf_union(std::invocable<double, double, double> auto... f) {
	return [=](double x, double y, double z) { return std::min({f(x, y, z)...}); };
};

constexpr auto sdf_rotate(std::invocable<double, double, double> auto f, double time) {
	return [=](double x, double y, double z) { 
		const auto sin_time = std::sin(time);
		const auto cos_time = std::cos(time);
		const auto camera_x = x * cos_time - z * sin_time;
		const auto camera_z = x * sin_time + z * cos_time;
		return f(camera_x, y, camera_z); 
	};
};

constexpr auto sdf_move(std::invocable<double, double, double> auto f, double time) {
	return [=](double x, double y, double z) { 
		const auto camera_x = x + std::cos(time);
		return f(camera_x, y, z); 
	};
};


constexpr std::tuple<double, double, double> unpack_coordinate(boxer::coordinate c) {
	const auto minmax = to_d(std::min(c.x_max, c.y_max));
	const auto dx = to_d(c.x_max) / (2 * minmax);
	const auto dy = to_d(c.y_max) / (2 * minmax);
	const auto x = (to_d(c.x) / minmax) - dx;
	const auto y = (to_d(c.y) / minmax) - dy;
	const auto time = to_d(c.z) / to_d(c.z_max) * 100;
	return std::tuple{x, y, time};
};

constexpr boxer::rgb_color color_shader(std::pair<double, unsigned> arg) {
	const auto [height, index] = arg;
	const auto intensity = std::clamp(0.0, height, 1.0);
	const auto τ = 2 * std::numbers::pi;
	const auto to_byte = [](double d) {
		return static_cast<std::uint8_t>(std::round(d * 255.0));
	};
	return {to_byte(intensity * 0.5 * (1 + std::sin(index))),
	        to_byte(intensity * 0.5 * (1 + std::sin(index + τ / 3))),
	        to_byte(intensity * 0.5 * (1 + std::sin(index + 2 * τ / 3)))};
}

constexpr auto make_shape_shader(double camera_z, unsigned max_steps = 30u) {
	return [=](boxer::coordinate c) {
		constexpr double ε = 0.001;
		const auto [x, y, time] = unpack_coordinate(c);
		const auto distance_function = sdf_union(
			sdf_rotate(torus_sdf(1), time),
			sdf_move(sdf_rotate(deathstar_sdf(2), time * -6), time));
		double ray_depth = camera_z;
		for (uint32_t i = 0; i < max_steps; ++i) {
			const auto [d, object] = distance_function(x, y, ray_depth);

			if (d <= 0.01) {
				double normal_x =
				        distance_function(x + ε, y, ray_depth).first -
				        distance_function(x - ε, y, ray_depth).first;
				double normal_y =
				        distance_function(x, y + ε, ray_depth).first -
				        distance_function(x, y - ε, ray_depth).first;
				double normal_z =
				        distance_function(x, y, ray_depth + ε).first -
				        distance_function(x, y, ray_depth - ε).first;

				double inverse_norm =
				        1.0 / dist(normal_x, normal_y, normal_z);
				normal_x *= inverse_norm;
				normal_y *= inverse_norm;
				normal_z *= inverse_norm;

				return std::pair{(normal_y + normal_x) / 4 + .5, object};
			} else {
				ray_depth += d;
			}
		}
		return std::pair{0.0, 0u};
	};
}

} // anonymous namespace

void play_rotationg_torus() {
	auto box = boxer::box{"Rotating Donut"};
	boxer::print_pixmaps(box, make_shape_shader(-2),
	                     color_shader, 0, 0, 2000);
}
