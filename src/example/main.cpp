#include <chrono>
#include <exception>
#include <thread>

#include <boxer/boxer.hpp>
#include <boxer/charts.hpp>
#include <boxer/debug.hpp>
#include <boxer/heatmap.hpp>
#include <boxer/table.hpp>

#include <clp/abbr.hpp>

#include "rotating_torus.hpp"

using namespace std::literals;

void styled_printing() {
	const auto box = boxer::box{"", boxer::double_box, fg(boxer::color::blue)};
	boxer::println("{} {} {} ({{{},{:5.4f}}})", "Some", "Test", "Text", 23, 42.0);
	boxer::println(fg(boxer::color::red), "{}", 42);
	boxer::println("Partially styled: {:+04}, with proper setback",
	               boxer::styled{23, fg(boxer::color::green) | boxer::font_style::bold});
}

void matrix_printing() {
	const auto box = boxer::box{"A Matrix", boxer::double_box,
	                            fg(boxer::color::bright_red) | bg(boxer::color::blue) |
	                                    boxer::font_style::italic};
	boxer::println("{}", boxer::to_matrix({{"1"}, {"", "1"}, {"", "", "1"}},
	                                      boxer::styles::bracket_matrix));
}

void difficult_table() {
	const auto box = boxer::box();
	boxer::println("On a terminal-emulator that fully supports unicode the following table "
	               "should be printed such that the first column is two columns wide and that "
	               "all the table-lines are uninterupted:\n\n");
	const auto data = std::vector<std::vector<std::string>>{
	        {"🏳️‍⚧️", "Transgender pride flag"},
	        {"⚧", "Transgender symbol"},
	        {"⚧️", "Transgender symbol emoji"},
	        {"🏳️‍🌈", "Rainbow flag"},
	        {"🇪🇺", "European flag"},
	        {"🇺🇦", "Ukrainian flag"},
	        {"🏴󠁧󠁢󠁳󠁣󠁴󠁿", "Scottish flag"},
	        {"の", "Hiragana"},
	        {"…", "Elipsis"},
	        {"𝑖", "Mathematical font versions"},
	        {"a̐ö̲", "Latin characters with combining characters"},
	        {"ẞ", "Capital ß"},
	        {"⟨⟩", "Angle brackets"},
	        {"␣", "Underbar"},
	        {"❤️", "Heart-emoji"},
	        {"👩🏼‍❤️‍👨🏻", "Couple emoji with skin-color"},
	        {"🙆🏿‍♂️", "Emoji with skin-clor and gender"},
	        {"👨‍👩‍👧‍👧", "Four member family"},
	};
	boxer::println("{}", boxer::to_table(data, "│", "│"));
}

void debug_scotland() {
	const auto box = boxer::box{"Debugging 🏴󠁧󠁢󠁳󠁣󠁴󠁿",
	                            boxer::font_style::normal_intensity | fg(boxer::color::gray),
	                            boxer::font_style::bold | fg(boxer::color::white)};
	boxer::debug::debug_width("🏴󠁧󠁢󠁳󠁣󠁴󠁿");
}

void parallel_printing(unsigned length, unsigned num_threads) {
	const auto box =
	        boxer::box{"printing very long strings", boxer::heavy_box, fg(boxer::color::red)};
	auto str = uct::multiply_string("👨‍👩‍👧‍👧⚧", length);
	auto threads = std::vector<std::jthread>{};
	for (auto i = 0u; i < num_threads; ++i) {
		threads.emplace_back([&, j = i] {
			for (auto i = 0u; i < 2u; ++i) {
				using namespace std::literals;
				std::this_thread::sleep_for(1ns);
				const auto box = boxer::exclusive_box{
				        std::format("Thread {}, {}", j, i), boxer::light_box,
				        fg(boxer::color::green)};
				boxer::println("{}", str);
			}
		});
	}
}

void print_tg_flag(std::string_view fill_string = "", unsigned bar_height = 5u) {
	const auto box = boxer::box{fill_string.size() ? fill_string : "🏳️‍⚧️"};
	const auto colors = {boxer::color::bright_blue, boxer::color::bright_red,
	                     boxer::color::bright_white, boxer::color::bright_red,
	                     boxer::color::bright_blue};
	const auto fill_width = uct::string_width(fill_string);
	const auto fill =
	        fill_width ? uct::multiply_string(fill_string,
	                                          boxer::global_boxer().text_width() / fill_width)
	                   : std::string{"\n"};
	const auto make_style = [&](boxer::color col) {
		using boxer::style_update;
		return fill_width ? style_update{fg(col)} : style_update{bg(col)};
	};
	for (const auto& color : colors) {
		for (auto i = 0u; i < bar_height; ++i) {
			boxer::println(make_style(color), "{}", fill);
		}
	}
}

void print_populations() {
	const auto data = std::vector<std::tuple<std::string, unsigned>>{
	        {{"🇪🇺", 446828803}, {"🇷🇺", 146424729}, {"🇹🇷", 85279553}, {"🇩🇪", 84358845},
	         {"🇫🇷", 68089000},  {"🇬🇧", 67026292},  {"🇮🇹", 58803163}, {"🇪🇸", 48196693},
	         {"🇺🇦", 41130432},  {"🇵🇱", 37749000},  {"🇰🇿", 19854083}, {"🇷🇴", 19053815},
	         {"🇳🇱", 17850301},  {"🇧🇪", 11697557},  {"🇨🇿", 10850620}, {"🇬🇷", 10678632},
	         {"🇸🇪", 10479625},  {"🇵🇹", 10444240},  {"🇦🇿", 10135373}, {"🇭🇺", 9689010},
	         {"🇧🇾", 9200600},   {"🇦🇹", 9120091},   {"🇨🇭", 8865270},  {"🇷🇸", 6690887},
	         {"🇧🇬", 6428318},   {"🇩🇰", 5883562},   {"🇫🇮", 5559198},  {"🇳🇴", 5488984},
	         {"🇸🇰", 5434712},   {"🇮🇪", 5149139},   {"🇭🇷", 3879074},  {"🇬🇪", 3688600},
	         {"🇧🇦", 3219415},   {"🇦🇲", 2961600},   {"🇱🇹", 2838389},  {"🇦🇱", 2793592},
	         {"🇲🇩", 2597100},   {"🇸🇮", 2107180},   {"🇱🇻", 1886300},  {"🇲🇰", 1836713},
	         {"🇽🇰", 1773971},   {"🇪🇪", 1331796},   {"🇨🇾", 918100},   {"🇱🇺", 645397},
	         {"🇲🇪", 617683},    {"🇲🇹", 520971},    {"🇮🇸", 385230},   {"🇦🇩", 80120},
	         {"🇱🇮", 39315},     {"🇲🇨", 39150},     {"🇸🇲", 33785},    {"🇻🇦", 799}}};
	boxer::print_vertical_chart(data, "European Populations");
}

void print_pixmaps() {
	auto box = boxer::box{"Pixmaps"};
	boxer::print_pixmaps(
	        box,
	        [](boxer::coordinate c) {
		        const auto x =
		                static_cast<double>(c.x) - static_cast<double>(c.x_max) / 2.0;
		        const auto y =
		                static_cast<double>(c.y) - static_cast<double>(c.y_max) / 2.0;
		        const auto z =
		                static_cast<double>(c.z) / static_cast<double>(c.z_max) * 10.0;
		        const auto d = std::sqrt(x * x + y * y);
		        return (1 + std::sin(d / std::numbers::pi - 2.0 * std::numbers::pi * z) *
		                            (5.0 / d)) /
		               2.0;
	        },
	        boxer::normal_float_to_height_color, 0, 0, 200);
}


int main(int argc, const char** argv) try {
	using namespace clp::abbr;
	auto parser = clp::arg_parser{
	        flg<"s,styled">{desc{"demonstrate styled printing"}},
	        flg<"m,matrix">{desc{"print a matrix"}},
	        flg<"d,difficult">{desc{"print a table containing multiple "
	                                "difficult graphemes"}},
	        flg<"🏴󠁧󠁢󠁳󠁣󠁴󠁿,scottland">{
	                desc{"print the decomposition of the scottish flag"}},
	        flg<"p,parallel_printing">{desc{"demonstrate printing from multiple threads"}},
	        flg<"🇪🇺,population_chart">{desc{"print a population chart of European countries"}},
	        def<"t,threads", unsigned>{2u, desc{"number of threads"}},
	        def<"l,length", unsigned>{100u, desc{"length of the long lines"}},
	        flg<"⚧,trans">{desc{"A short option that is not an ascii-character"}},
	        flg<"🏳️‍⚧️,trans2">{
	                desc{"A short option that is a complex emoji-sequence"}},
	        flg<"h,help">{desc{"print this help and exit."}},
	        flg<"o,torus">{desc{"display a torus-video."}},
	        flg<"P,pixmaps">{desc{"display a pixmap-video."}},
	};
	const auto parsed = parser.parse(argc, argv);

	if (parsed.get<"help">()) {
		const auto box = boxer::box{"", boxer::light_box};
		boxer::println("{}", parser.generate_help(argv[0]));
		return 0;
	}

	if (parsed.get<"styled">()) {
		styled_printing();
	}
	if (parsed.get<"matrix">()) {
		matrix_printing();
	}
	if (parsed.get<"difficult">()) {
		difficult_table();
	}
	if (parsed.get<"scottland">()) {
		debug_scotland();
	}
	if (parsed.get<"parallel_printing">()) {
		parallel_printing(parsed.get<"length">(), parsed.get<"threads">());
	}
	if (parsed.get<"population_chart">()) {
		print_populations();
	}
	if (parsed.get<"trans2">()) {
		print_tg_flag();
	}
	if (parsed.get<"trans">()) {
		print_tg_flag("⚧");
	}
	if (parsed.get<"pixmaps">()) {
		print_pixmaps();
	}
	if (parsed.get<"torus">()) {
		play_rotationg_torus();
	}

} catch (std::exception& e) {
	const auto box = boxer::box{"Exception", boxer::double_box, fg(boxer::color::bright_red),
	                            boxer::font_style::bold};
	boxer::println("{}", e.what());
} catch (const char* s) {
	const auto box = boxer::box{"Exception", boxer::double_box, fg(boxer::color::bright_red),
	                            boxer::font_style::bold};
	boxer::println("{}", s);
}
