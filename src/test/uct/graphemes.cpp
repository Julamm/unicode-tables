#include <uct/graphemes.hpp>

#include <catch2/catch.hpp>

#include <string_view>

using namespace std::literals;

static_assert(uct::peel_grapheme("ab") == std::pair{uct::grapheme{"a",1},"b"sv});
static_assert(uct::peel_grapheme("a") == std::pair{uct::grapheme{"a",1},""sv});
static_assert(uct::peel_grapheme("\n") == std::pair{uct::grapheme{"\n",0},""sv});
static_assert(uct::peel_grapheme("⚧\n") == std::pair{uct::grapheme{"⚧",1},"\n"sv});
static_assert(uct::peel_grapheme("🏳️‍⚧️X") == std::pair{uct::grapheme{"🏳️‍⚧️",2},"X"sv});
static_assert(uct::peel_grapheme("🇪🇺X") == std::pair{uct::grapheme{"🇪🇺",2},"X"sv});
static_assert(uct::peel_grapheme("äb") == std::pair{uct::grapheme{"ä",1},"b"sv});
static_assert(uct::peel_grapheme("X́ab") == std::pair{uct::grapheme{"X́",1},"ab"sv});
static_assert(uct::peel_grapheme("X́̉ab") == std::pair{uct::grapheme{"X́̉",1},"ab"sv});

