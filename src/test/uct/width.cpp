#include <uct/width.hpp>

static_assert(uct::char_width(U'a') == 1);
static_assert(uct::char_width(U'ä') == 1);
static_assert(uct::char_width(U'𝛼') == 1);
static_assert(uct::char_width(U'の') == 2);

static_assert(uct::string_width("") == 0);
static_assert(uct::string_width("a") == 1);
static_assert(uct::string_width("ä") == 1);
static_assert(uct::string_width("𝛼") == 1);
static_assert(uct::string_width("の") == 2);

static_assert(uct::string_width("🏳️‍⚧️") == 2);
static_assert(uct::string_width("🏴󠁧󠁢󠁳󠁣󠁴󠁿") == 2);
static_assert(uct::string_width("👩🏼‍❤️‍👨🏻") == 2);
static_assert(uct::string_width("🇪🇺") == 2);
static_assert(uct::string_width("🙆🏿‍♂️") == 2);
static_assert(uct::string_width("ẞ ") == 2);
static_assert(uct::string_width("🏳️‍🌈") == 2);
static_assert(uct::string_width("🇺🇦") == 2);
static_assert(uct::string_width("👨‍👩‍👧‍👧") == 2);
static_assert(uct::string_width("❤️") == 1);
static_assert(uct::string_width("…⟨ℒ⟩…a̐éö̲␣") == 9);

static_assert(uct::string_width("\033[31mX") == 1);
