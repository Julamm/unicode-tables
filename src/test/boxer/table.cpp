#include <boxer/table.hpp>

#include <catch2/catch.hpp>

#include <iostream>

void print_spaces(const std::string& s) {
	auto s2 = std::string{};
	for (auto c: s) {
		if (c == ' ') {
			s2 += "␣";
		} else {
			s2 += c;
		}
	}
	std::cout << s2 << '\n';
}

TEST_CASE("basic prealigned", "") {
	const auto table = boxer::to_table({{"a", "bc", "d"}, {"a", "bc", "d"}});
	const auto expected = " a bc d \n a bc d \n";
	CHECK(table == expected);
}

TEST_CASE("basic missaligned", "") {
	const auto table = boxer::to_table({{"a", "bc", "d"}, {"ab", "", "cd"}});
	const auto expected = " a  bc d  \n ab    cd \n";
	CHECK(table == expected);
}

TEST_CASE("unicode missaligned", "") {
	const auto table = boxer::to_table({{"…","の","ℒ", "ö̲", "❤️","a"}, {""}}, "|");
	const auto expected = "|…|の|ℒ|ö̲|❤️|a|\n| |  | | | | |\n";
	CHECK(table == expected);
}
