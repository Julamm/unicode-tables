#include <boxer/boxer.hpp>
#include <uct/width.hpp>

#include <vector>
#include <string>

int main(int argc, char** argv) {
	const auto args = std::vector<std::string>(argv+1, argv+argc);

	for (const auto& arg: args) {
		boxer::println("Length(“{}”) = {}", arg, uct::string_width(arg));
	}
}
