#ifndef UCT_STRING_UTILS_HPP
#define UCT_STRING_UTILS_HPP

#include <algorithm>
#include <bit>
#include <cassert>
#include <cstdint>
#include <format>
#include <optional>
#include <span>
#include <stdexcept>
#include <string_view>
#include <tuple>

namespace uct {

constexpr std::string multiply_string(std::string_view str, std::size_t n) {
	auto ret = std::string{};
	ret.reserve(str.size() * n);
	for (auto i = std::size_t{}; i < n; ++i) {
		ret += str;
	}
	return ret;
}
} // namespace uct

namespace uct::impl {
constexpr auto to_uchar(char c) { return static_cast<unsigned char>(c); }

constexpr auto parse_continuation_char(char c) -> char32_t {
	if (std::countl_one(to_uchar(c)) != 1) {
		throw std::runtime_error{"Invalid UTF-8 continuation-character"};
	}
	using uchar = unsigned char;
	return static_cast<char32_t>(to_uchar(c) bitand uchar{0b00111111});
}

constexpr auto peel_char(std::string_view s) -> std::tuple<char32_t, std::string_view> {
	using uchar = unsigned char;
	if (s.empty()) {
		throw std::invalid_argument{"Peel_char may only be called with non-empty strings, "
		                            "but was called with an empty one."};
	}
	const auto leading_byte = to_uchar(s.front());
	const auto leading_ones = static_cast<unsigned>(std::countl_one(leading_byte));
	if (leading_ones == 0) {
		return {leading_byte, s.substr(1)};
	}
	if (leading_ones == 1 or leading_ones > 4 or s.size() < leading_ones) {
		throw std::runtime_error{
		        std::format("peel_char called with invalid utf-8 (“{}”)", s)};
	}
	auto ret = char32_t{leading_byte} bitand (uchar{0b1111111} >> leading_ones);
	for (auto i = 1u; i < leading_ones; ++i) {
		ret <<= 6;
		ret += parse_continuation_char(s.at(i));
	}
	return {ret, s.substr(leading_ones)};
}

template <typename T, std::size_t N>
constexpr auto find(char32_t c, const std::array<std::tuple<char32_t, char32_t, T>, N>& haystack)
        -> std::optional<T> {
	const auto get_first = [](const auto& t) { return std::get<0>(t); };
	assert(std::ranges::is_sorted(haystack));
	auto it = std::ranges::upper_bound(haystack, c, {}, get_first);
	if (it == haystack.begin()) {
		return std::nullopt;
	}
	assert(it != haystack.begin());
	--it;
	const auto [low, high, w] = *it;
	return high >= c ? std::optional{w} : std::nullopt;
}

template <typename T, std::size_t N>
constexpr auto in(char32_t c, const std::array<std::tuple<char32_t, char32_t, T>, N>& haystack)
        -> bool {
	return find(c, haystack) != std::nullopt;
}

} // namespace uct::impl

#endif
