#ifndef UCT_GRAPHEMES_HPP
#define UCT_GRAPHEMES_HPP

#include <cassert>
#include <format>
#include <string_view>
#include <utility>

#include <uct/width.hpp>

namespace uct {

struct grapheme {
	std::string_view str;
	std::size_t width;

	constexpr bool friend operator ==(const uct::grapheme& lhs, const uct::grapheme& rhs) =default;
};

namespace impl {
constexpr bool is_regional_indicator(char32_t c) {return 0x1F1E6 <= c and c <= 0x1F1FF; }

constexpr std::pair<grapheme, std::string_view>
peel_flag(char32_t first_half, std::string_view tail, std::string_view::const_iterator flag_start) {
	const auto [second_half, return_tail] = impl::peel_char(tail);
	if (not is_regional_indicator(second_half)) {
		throw std::runtime_error(std::format(
		        "Bad regional indicator (“flag-emoji”): Starts with {:04x} (“{}”), but "
		        "continues with {:04x}",
		        unsigned{first_half}, (first_half - 0x1f1e6) + 'A', unsigned{second_half}));
	}
	return {{{flag_start, return_tail.begin()}, 2}, return_tail};
}
} // namespace impl

inline constexpr bool is_newline(char32_t c) { return c == '\n'; }

constexpr std::pair<grapheme, std::string_view> peel_grapheme(std::string_view s,
                                                              std::size_t emoji_width = 2) {
	assert(not s.empty());
	if (const auto [emoji, tail] = impl::peel_emoji(s); not emoji.empty()) {
		return {{emoji, emoji_width}, tail};
	}
	auto consume_next = true;
	auto width = std::size_t{};
	auto consumed = std::size_t{};
	auto start = s.begin();
	while (not s.empty()) {
		const auto [c, tail] = impl::peel_char(s);
		if (impl::is_regional_indicator(c)) {
			if (consumed) {
				return {{{start, s.begin()}, width}, s};
			} else {
				return impl::peel_flag(c, tail, start);
			}
		}
		const auto w = char_width(c);
		const auto is_nl = is_newline(c);
		if (consumed == 0 and is_nl) {
			return {{{start, tail.begin()}, width}, tail};
		}
		if ((w and not consume_next) or is_nl) {
			return {{{start, s.begin()}, width}, s};
		}
		if (w) {
			consume_next = false;
			// TODO: deal with joiners.
		}
		s = tail;
		width += w;
		++consumed;
	}
	const auto ret = std::string_view(start, s.begin());
	return std::pair{grapheme{ret, width}, s};
}

} // namespace uct

#endif // UCT_GRAPHEMES_HPP
