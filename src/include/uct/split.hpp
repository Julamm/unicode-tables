#ifndef UCT_SPLIT_HPP
#define UCT_SPLIT_HPP

#include <string_view>
#include <tuple>
#include <vector>

#include <uct/graphemes.hpp>

namespace uct {

namespace impl {
inline constexpr bool is_newline(std::string_view s) { return s == "\n"; }
} // namespace impl

inline constexpr std::vector<std::string_view> split_lines(std::string_view s,
                                                           std::size_t max_width) {
	auto ret = std::vector<std::string_view>{};
	auto start = s.begin();
	auto current_width = std::size_t{};
	while (not s.empty()) {
		auto [grapheme, tail] = peel_grapheme(s);
		s = tail;
		if (impl::is_newline(grapheme.str)) {
			ret.emplace_back(start, grapheme.str.begin());
			if (not s.empty()) {
				std::tie(grapheme, tail) = peel_grapheme(s);
				start = grapheme.str.begin();
				current_width = grapheme.width;
			} else {
				return ret;
			}
		} else if (current_width + grapheme.width > max_width) {
			ret.emplace_back(start, grapheme.str.begin());
			start = grapheme.str.begin();
			current_width = grapheme.width;
		} else {
			current_width += grapheme.width;
		}
	}
	if (start != s.end()) {
		ret.emplace_back(start, s.end());
	}
	return ret;
}

} // namespace uct

#endif // UCT_SPLIT_HPP

