#ifndef BOXER_DEBUG_HPP
#define BOXER_DEBUG_HPP

#include <format>

#include <uct/string_utils.hpp>
#include <boxer/boxer.hpp>
#include <uct/width.hpp>

namespace boxer::debug {

inline void debug_width(std::string_view s) {
	while (not s.empty()) {
		const auto [e, tail] = uct::impl::peel_emoji(s);
		if (not e.empty()) {

			::boxer::println("Emoji: “{}”, width = emoji-width (defaults to 2)\n", e);
			s = tail;
		} else {
			const auto [c, s_] = uct::impl::peel_char(s);
			s = s_;
			::boxer::println("“{:x}”, width = {}\n", unsigned{+c},uct::char_width(c));
		}
	}
}

} // namespace boxer::debug

#endif
