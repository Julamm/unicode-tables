#ifndef BOXER_BOXER_HPP
#define BOXER_BOXER_HPP

#include <format>
#include <iostream>
#include <mutex>
#include <ostream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

#include <boxer/styles.hpp>
#include <boxer/format_styles.hpp>

namespace boxer {

struct box_style {
	std::string_view h;
	std::string_view v;
	std::string_view tl;
	std::string_view tr;
	std::string_view bl;
	std::string_view br;
	std::string_view l;
	std::string_view r;
};

constexpr auto no_box = box_style{};
constexpr auto light_box = box_style{"─", "│", "┌", "┐", "└", "┘", "├", "┤"};
constexpr auto rounded_box = box_style{"─", "│", "╭", "╮", "╰", "╯", "├", "┤"};
constexpr auto heavy_box = box_style{"━", "┃", "┏", "┓", "┗", "┛", "┣", "┫"};
constexpr auto double_box = box_style{"═", "║", "╔", "╗", "╚", "╝", "╠", "╣"};
constexpr auto block_box = box_style{"█", "█", "█", "█", "█", "█", "█", "█"};
constexpr auto ascii_box = box_style{"-", "|", "+", "+", "+", "+", "|", "|"};
constexpr auto default_box = rounded_box;

struct style {
	box_style bs;
	text_style ts;
};

class boxer_error : public std::runtime_error {
	using std::runtime_error::runtime_error;
};

struct terminal_dimensions {
	unsigned width;
	unsigned height;
};

terminal_dimensions get_terminal_dimensions();

class boxer {
public:
	boxer(std::ostream& out = std::cout);
	boxer(std::ostream&&) = delete;

	void start_box(std::string_view label = "", const box_style& style = default_box,
	               style_update su = {}, style_update header_style = {});
	void end_box();
	void rule();
	void end_all_boxes();
	// void print(std::string_view s) const;
	template <class... Args>
	void println(std::format_string<Args...> format, Args&&... args) {
		auto lock = std::unique_lock{m_mutex};
		print_string(std::format(
		        format, add_style_context(std::forward<Args>(args), get_style().ts)...));
	}
	template <class... Args>
	void println(style_update su, std::format_string<Args...> format, Args&&... args) {
		auto lock = std::unique_lock{m_mutex};
		print_string(su, std::format(format, add_style_context(std::forward<Args>(args),
		                                                       get_style().ts | su)...));
	}
	std::string read(std::string_view prompt);

	style get_style() const {
		if (m_active_boxes.empty()) {
			return {};
		} else {
			return m_active_boxes.back();
		}
	}

	std::size_t text_width() const;

	void unchecked_print_single_line(std::string_view s);
	void unchecked_print(std::string_view s);

	std::size_t border_width() const {return m_active_boxes.size();}

	friend class exclusive_box;

private:
	void print_fitting_line(std::string_view s, std::size_t w);
	void print_string(std::string_view s);
	void print_fitting_line(text_style, std::string_view s, std::size_t w);
	void print_string(style_update, std::string_view s);

	std::ostream* m_out;
	unsigned m_width = 0u;
	std::vector<style> m_active_boxes = {};
	bool m_disable_boxes = false;
	std::recursive_mutex m_mutex;
};

inline boxer& global_boxer() {
	static auto b = boxer{};
	return b;
}

class box {
public:
	box(std::string_view label = "", box_style style = default_box, style_update su = {},
	    style_update header_style = {})
	        : box{label, style, global_boxer(), su, header_style} {}
	box(std::string_view label, style_update su, style_update header_style = {})
	        : box{label, default_box, global_boxer(), su, header_style} {}
	box(std::string_view label, box_style style, boxer& b, style_update su = {},
	    style_update header_style = {})
	        : m_boxer{&b} {
		b.start_box(label, style, su, header_style);
	}

	~box() { m_boxer->end_box(); }

	void rule() const { m_boxer->rule(); }

	template <typename... Args>
	void println(std::format_string<Args...> format, Args&&... args) {
		m_boxer->println(format, std::forward<Args>(args)...);
	}

	template <typename... Args>
	void println(style_update su, std::format_string<Args...> format, Args&&... args) {
		m_boxer->println(su, format, std::forward<Args>(args)...);
	}

	std::size_t text_width() const {return m_boxer->text_width();}

	void unchecked_print_single_line(std::string_view s) {m_boxer->unchecked_print_single_line(s);}
	void unchecked_print(std::string_view s){m_boxer->unchecked_print(s);}

	std::size_t border_width() const {return m_boxer->border_width();}
private:
	boxer* m_boxer;
};

class exclusive_box {
public:
	exclusive_box(std::string_view label = "", box_style style = default_box,
	              style_update su = {}, style_update header_style = {})
	        : exclusive_box{label, style, global_boxer(), su, header_style} {}
	exclusive_box(std::string_view label, style_update su, style_update header_style = {})
	        : exclusive_box{label, default_box, global_boxer(), su, header_style} {}
	exclusive_box(std::string_view label, box_style style, boxer& b, style_update su = {},
	              style_update header_style = {})
	        : m_boxer{&b}, m_lock{b.m_mutex} {
		b.start_box(label, style, su, header_style);
	}

	~exclusive_box() { m_boxer->end_box(); }

	void rule() const { m_boxer->rule(); }

	template <typename... Args>
	void println(std::format_string<Args...> format, Args&&... args) {
		m_boxer->println(format, std::forward<Args>(args)...);
	}

	template <typename... Args>
	void println(style_update su, std::format_string<Args...> format, Args&&... args) {
		m_boxer->println(su, format, std::forward<Args>(args)...);
	}

	std::size_t text_width() const {return m_boxer->text_width();}

	void unchecked_print_single_line(std::string_view s) {m_boxer->unchecked_print_single_line(s);}
	void unchecked_print(std::string_view s){m_boxer->unchecked_print(s);}

	std::size_t border_width() const {return m_boxer->border_width();}
private:
	boxer* m_boxer;
	std::unique_lock<std::recursive_mutex> m_lock;
};

template <typename... Args>
void println(std::format_string<Args...> format, Args&&... args) {
	global_boxer().println(format, std::forward<Args>(args)...);
}

template <typename... Args>
void println(style_update su, std::format_string<Args...> format, Args&&... args) {
	global_boxer().println(su, format, std::forward<Args>(args)...);
}

inline void rule() { global_boxer().rule(); }

} // namespace boxer

#endif
