#ifndef BOXER_HEATMAP_HPP
#define BOXER_HEATMAP_HPP

#include <algorithm>
#include <chrono>
#include <cmath>
#include <ranges>
#include <span>
#include <thread>

#include <uct/string_utils.hpp>
#include <uct/width.hpp>

#include <boxer/boxer.hpp>
#include <boxer/styles.hpp>
#include <variant>

namespace boxer {

inline rgb_color normal_float_to_height_color(double value) {
	if (value < 0.0) {
		return {0, 0, 255};
	} else if (value > 1.0) {
		return {255, 255, 255};
	} else {
		return {0, static_cast<std::uint8_t>(std::round(value * 255.0)), 0};
	}
}

struct coordinate {
	std::size_t x;
	std::size_t x_max;
	std::size_t y;
	std::size_t y_max;
	std::size_t z = 0u;
	std::size_t z_max = 1u;
};

void print_heatmap(box& box, std::invocable<coordinate> auto f,
                   std::invocable<decltype(f(std::declval<coordinate>()))> auto to_color,
                   std::size_t width = 0u, std::size_t height = 80u,
                   std::size_t z_max_passthrough = 1u) {
	const auto text_width = box.text_width();
	if (width > text_width) {
		throw boxer_error{"requested width of heatmap wider than display-area"};
	}
	width = (width != 0) ? width : text_width;
	height = (height != 0) ? height : get_terminal_dimensions().height - 2u;
	auto line = std::string{};
	const auto padding = std::string(text_width - width, ' ');
	for (auto y = height + 1u; y > 1u; y -= 2u) {
		line.clear();
		for (auto x = std::size_t{}; x < width; ++x) {
			const auto col_top =
			        to_color(f({x, width, y - 1u, height, 0u, z_max_passthrough}));
			if (y == 2) {
				line += to_escape_code(text_style{} | fg(col_top));
			} else {
				const auto col_bot = to_color(
				        f({x, width, y - 2u, height, 0u, z_max_passthrough}));
				line += to_escape_code(text_style{} | fg(col_top) | bg(col_bot));
			}
			line += "▀";
		}
		line += reset_code();
		line += padding;
		box.unchecked_print_single_line(line);
	}
}

void print_pixmaps(box& box, std::invocable<coordinate> auto f,
                   std::invocable<decltype(f(std::declval<coordinate>()))> auto to_color,
                   std::size_t width = 0u, std::size_t height = 80u, std::size_t n = 1,
                   std::chrono::milliseconds delay = std::chrono::milliseconds{0}) {
	const auto text_width = box.text_width();
	if (width > text_width) {
		throw boxer_error{"requested width of heatmap wider than display-area"};
	}
	width = (width != 0) ? width : text_width;
	height = (height != 0) ? height : 2u * get_terminal_dimensions().height - 4u;
	print_heatmap(box, f, to_color, width, height, n);
	auto data = std::string{};
	for (auto z = std::size_t{1}; z < n; ++z) {
		std::this_thread::sleep_for(delay);
		data = std::format("\033[{}A\033[{}C", (height + 1u) / 2, box.border_width());
		for (auto y = height + 1u; y > 1u; y -= 2u) {
			for (auto x = std::size_t{}; x < width; ++x) {
				const auto col_top = to_color(f({x, width, y - 1, height, z, n}));
				if (y == 2) {
					data += to_escape_code(text_style{} | fg(col_top));
				} else {
					const auto col_bot =
					        to_color(f({x, width, y - 2, height, z, n}));
					data += to_escape_code(text_style{} | fg(col_top) |
					                       bg(col_bot));
				}
				data += "▀";
			}
			data += reset_code();
			data += std::format("\033[1B\033[{}D", width);
		}
		data += "\033[1E";
		box.unchecked_print(data);
	}
}

} // namespace boxer

void temp();

#endif // BOXER_HEATMAP_HPP
