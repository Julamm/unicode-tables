#ifndef CLP_TYPE_SUPPORT_HPP
#define CLP_TYPE_SUPPORT_HPP

#include <cassert>
#include <charconv>
#include <optional>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

namespace clp {

class bad_argument_list : public std::runtime_error {
public:
	using std::runtime_error::runtime_error;
};

class empty_argument_list : public bad_argument_list {
	using bad_argument_list::bad_argument_list;
};

class bad_argument : public bad_argument_list {
	using bad_argument_list::bad_argument_list;
};

template <typename T>
struct parse_type {};

namespace impl {

template <typename Char, typename Traits, typename Allocator>
inline std::string type_to_string(parse_type<std::basic_string<Char, Traits, Allocator>>, int) {
	return "string";
}

inline std::string type_to_string(parse_type<bool>, int) { return "bool"; }

template <typename T, std::enable_if_t<std::is_integral_v<T>, int> = 0>
std::string type_to_string(parse_type<T>, int) {
	return "integer";
}

template <typename T, std::enable_if_t<std::is_floating_point_v<T>, int> = 0>
std::string type_to_string(parse_type<T>, int) {
	return "float";
}

template <typename T>
std::string type_to_string(parse_type<T>, ...) {
	return "?";
}

template <typename T>
std::string type_to_string(parse_type<std::optional<T>>, int) {
	return type_to_string(parse_type<T>{}, 0);
}

template <typename T,
          std::enable_if_t<std::is_same_v<std::ostream&, decltype(std::declval<std::ostream&>()
                                                                  << std::declval<const T&>())>,
                           int> = 0>
std::string value_to_string(const T& value, int) {
	auto s = std::ostringstream{};
	s << value;
	return s.str();
}

template <typename T>
std::string value_to_string(const T&, ...) {
	return "";
}

inline std::string parse_argument(parse_type<std::string>, std::string arg, const std::string&,
                                  int) {
	return arg;
}

inline bool parse_argument(parse_type<bool>, std::string arg, const std::string& param_name, int) {
	if (arg == "0" or arg == "false") {
		return false;
	} else if (arg == "1" or arg == "true") {
		return true;
	} else {
		throw bad_argument{param_name + " (\"" + arg + "\") is not a boolean value"};
	}
}

template <typename T, std::enable_if_t<std::is_integral_v<T>, int> = 0>
T parse_argument(parse_type<T>, std::string arg, const std::string& param_name, int) {
	auto ret = T{};
	auto begin = arg.c_str();
	auto end = begin + arg.size();
	const auto [ptr, ec] = std::from_chars(begin, end, ret);
	if (ec == std::errc::result_out_of_range) {
		throw bad_argument{param_name + " (" + arg + ") is out of range"};
	} else if (ptr != end or arg == "") {
		throw bad_argument{param_name + " (\"" + arg + "\") is not a valid integer"};
	}
	return ret;
}

// apparently libstdc++ doesn't yet implement from_chars for floats:
template <typename T, std::enable_if_t<std::is_floating_point_v<T>, int> = 0>
T parse_argument(parse_type<T>, std::string arg, const std::string&, int) {
	return static_cast<T>(std::stold(arg));
}

// Generic fallback:
template <typename T>
T parse_argument(parse_type<T>, std::string arg, const std::string&, ...) {
	auto stream = std::istringstream{arg};
	auto ret = T{};
	stream >> ret;
	return ret;
}

} // namespace impl

} // namespace clp

#endif
