#! /usr/bin/python3


from typing import Iterable


def zero_width(id: int, id_: int, line: list[str]) -> bool:
    assert (id == id_)
    return line[1] in ['Cc', 'Cf', 'Cs', 'Mn', 'Me'] \
        or 0x1160 <= id <= 0x11ff \
        or id in [0xad, 0x200b]


def is_emoji(low: int, high: int, components: list[str]) -> bool:
    if low == 0x1F1E6:  # Regional indicator symbols
        assert high == 0x1F1FF
        # This is technically not true, but since these are always supposed to
        # come in pairs, assigning them width=1, produces a correct total width
        # of 2:
        return False
    else:
        return bool(components) and components[0] == "Emoji_Presentation"


def parse_range(s: str, label: int) -> tuple[int, int, int]:
    parts = s.split('..')
    assert 1 <= len(parts) <= 2
    if len(parts) == 1:
        n = int(parts[0], base=16)
        return n, n, label
    else:
        return int(parts[0], base=16), int(parts[1], base=16), label


def parse_file(filename: str) -> Iterable[tuple[int, int, list[str]]]:
    for line in open(filename):
        if line and line[-1] == '\n':
            line = line[:-1]
        line = line.split("#", 1)[0]
        if not line:
            continue
        components = [c.strip() for c in line.split(';')]
        start, end, _ = parse_range(components[0], 0)
        yield (start, end, components[1:])


def merge(lst: list[tuple[int, int, int]]
          ) -> list[tuple[int, int, int]]:
    ret = []
    current_start, current_end, label = lst[0]
    for s, e, l in lst[1:]:
        if current_end + 1 >= s and label == l:
            current_end = max(current_end, e)
        else:
            ret.append((current_start, current_end, label))
            current_start, current_end, label = s, e, l
    ret.append((current_start, current_end, label))
    return ret


def main(args: list[str]) -> int:
    zero_width_filename = 'UnicodeData.txt' if len(args) <= 1 else args[1]
    zero_width_chars = ([(low, high, 0) for low, high, components in parse_file(
        zero_width_filename) if zero_width(low, high, components)])

    asian_width_filename = 'EastAsianWidth.txt' if len(args) <= 2 else args[2]
    asian = ([(l, h, 2) for l, h, components in parse_file(
        asian_width_filename) if components[0] in ['W', 'F']])

    emoji_filename = 'emoji-data.txt' if len(args) <= 3 else args[3]
    emoji = ([(l, h, 2) for l, h, components in parse_file(
        emoji_filename) if is_emoji(l, h, components)])
    full_data = merge(list(sorted(zero_width_chars + asian + emoji)))
    formated = (
        '\n'.join(
            (f'{{{{0x{start:04x}}}, {{0x{end:04x}}}, {w}}},' for start,
             end,
             w in full_data)))
    out_filename = "unicode_widths.txt" if len(args) <= 4 else args[4]
    with open(out_filename, 'w') as file:
        file.write(formated)
    return 0


if __name__ == "__main__":
    import sys
    sys.exit(main(sys.argv))
