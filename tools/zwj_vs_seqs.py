#! /usr/bin/python3

from typing import DefaultDict

from dataclasses import dataclass, field

from collections import defaultdict


@dataclass
class State:
    codepoint: int
    next_start: int
    next_end: int
    succs: dict[int, int] = field(default_factory=lambda: {})


def state_to_str(s: State) -> str:
    ret = f'/* {s.codepoint:03x} */ {len(s.succs)}'
    if s.succs:
        ret +=', ' + ', '.join(
            (f'0x{(char << 12) | index:08x}' for char, index in sorted(s.succs.items())))
    return ret


FiniteStateMachine = list[State]


def fsm_to_str(fsm: FiniteStateMachine) -> str:
    return ",\n".join((state_to_str(e) for e in fsm))


ZWJ = 'FE0F'


def emoji_starts(l: list[list[int]]) -> str:
    return ", ".join(('0x' + hex(e)
                     for e in sorted(list({seq[0] for seq in l if seq}))))


def generate_fsm(l: list[list[int]]) -> FiniteStateMachine:
    prefixes: dict[tuple[int, ...], set[int]] = DefaultDict(lambda: set())
    for seq in l:
        for i in range(len(seq)):
            prefixes[tuple(seq[0:i])].add(seq[i] if len(seq) > i else 0)
    ret = [State(0, 1, 1 + len(prefixes[tuple()]))]

    def add(ret: list[State], prefix: tuple[int, ...],
            parent_index: int) -> None:
        next = sorted(list(prefixes[prefix]))
        start_index = len(ret)
        end_index = start_index + len(next)
        ret[parent_index].next_start = start_index
        ret[parent_index].next_end = end_index
        ret += [State(c, 0, 0) for c in next]
        for i, c in enumerate(next):
            pos = start_index + i
            if c == 0:
                ret[pos] = State(c, pos, pos)
            else:
                add(ret, prefix + (c,), pos)

    add(ret, tuple(), 0)
    return ret


def are_equivalent(i: int, j: int, fsm: FiniteStateMachine,
                   equivalent_states: defaultdict[int, set[int]]) -> bool:
    i_successors = {
        fsm[k].codepoint: k for k in range(
            fsm[i].next_start,
            fsm[i].next_end)}
    j_successors = {
        fsm[k].codepoint: k for k in range(
            fsm[j].next_start,
            fsm[j].next_end)}
    if i_successors.keys() != j_successors.keys():
        return False
    for k in i_successors.keys():
        if j_successors[k] not in equivalent_states[i_successors[k]]:
            return False
    return True


def compute_relabeling(
        n: int, d: defaultdict[int, set[int]]) -> dict[int, int]:
    ret = dict[int, int]()
    for i in range(n):
        s = d[i]
        ret[i] = min(i, min(s)) if s else i
    return ret


def remove_duplicates(fsm: FiniteStateMachine,
                      label_dict: dict[int, int]) -> FiniteStateMachine:
    new_label_dict = dict[int, int]()
    counter = 0
    for k, v in label_dict.items():
        if k == v:
            new_label_dict[k] = counter
            counter += 1 + len(fsm[k].succs)
    ret = []
    for k, v in label_dict.items():
        if k == v:
            old_state = fsm[k]
            ret.append(State(new_label_dict[k],
                             new_label_dict[label_dict[old_state.next_start]],
                             new_label_dict[label_dict[old_state.next_end]],
                             {cp: new_label_dict[index] for cp,
                              index in old_state.succs.items()}))
    return ret


def minimize_fsm(fsm: FiniteStateMachine) -> FiniteStateMachine:
    equivalent_states = defaultdict[int, set[int]](lambda: set())
    equivalent_states[0].add(0)
    continue_flag = True
    while continue_flag:
        continue_flag = False
        for i in range(len(fsm)):
            for j in range(i + 1, len(fsm)):
                if j in equivalent_states[i]:
                    continue
                if are_equivalent(i, j, fsm, equivalent_states):
                    equivalent_states[i].add(j)
                    equivalent_states[j].add(i)
                    continue_flag = True
                    # print(f"States {i} and {j} are equivalent")
    label_dict = compute_relabeling(len(fsm), equivalent_states)
    for state in fsm:
        for i in range(state.next_start, state.next_end):
            state.succs[fsm[i].codepoint] = label_dict[i]
    fsm = remove_duplicates(fsm, label_dict)
    return fsm


def main(args: list[str]) -> int:
    filename = 'emoji-zwj-sequences.txt' if len(args) <= 1 else args[1]
    data = [[int(x, 16) for x in line.split('#', 1)[0].split(';')
             [0].split()] + [0] for line in open(filename)]
    # print("Emoji-starts:", emoji_starts(data))
    fsm = generate_fsm(data)
    fsm = minimize_fsm(fsm)
    print(fsm_to_str(fsm))
    return 0


if __name__ == "__main__":
    import sys
    sys.exit(main(sys.argv))
